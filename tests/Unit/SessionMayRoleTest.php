<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use Foodsharing\Lib\Session;
use Foodsharing\Modules\Core\DBConstants\Foodsaver\Role;
use Foodsharing\Modules\Foodsaver\FoodsaverGateway;
use Tests\Support\UnitTester;

class SessionTestAdapter extends Session
{
    public function __construct(
        private readonly FoodsaverGateway $foodsaverGateway,
        protected bool $initialized = false
    ) {
        parent::__construct($foodsaverGateway, $initialized);
    }

    public function setTestUser(Role $role)
    {
        $this->setId(1);
        $this->setAuthLevel($role);
    }
}

class SessionMayRoleTest extends Unit
{
    private ?SessionTestAdapter $session = null;
    protected UnitTester $tester;

    public function _before()
    {
        $this->session = new SessionTestAdapter(
            $this->tester->get(FoodsaverGateway::class),
            true
        );
    }

    public function testDifferentRolesAndPermissions(): void
    {
        $this->session->setTestUser(Role::SITE_ADMIN);
        $this->assertTrue($this->session->mayRole(Role::FOODSHARER));
        $this->assertTrue($this->session->mayRole(Role::FOODSAVER));
        $this->assertTrue($this->session->mayRole(Role::STORE_MANAGER));
        $this->assertTrue($this->session->mayRole(Role::AMBASSADOR));
        $this->assertTrue($this->session->mayRole(Role::ORGA));
        $this->assertTrue($this->session->mayRole(Role::SITE_ADMIN));

        $this->session->setTestUser(Role::ORGA);
        $this->assertTrue($this->session->mayRole(Role::FOODSHARER));
        $this->assertTrue($this->session->mayRole(Role::FOODSAVER));
        $this->assertTrue($this->session->mayRole(Role::STORE_MANAGER));
        $this->assertTrue($this->session->mayRole(Role::AMBASSADOR));
        $this->assertTrue($this->session->mayRole(Role::ORGA));
        $this->assertFalse($this->session->mayRole(Role::SITE_ADMIN));

        $this->session->setTestUser(Role::AMBASSADOR);
        $this->assertTrue($this->session->mayRole(Role::FOODSHARER));
        $this->assertTrue($this->session->mayRole(Role::FOODSAVER));
        $this->assertTrue($this->session->mayRole(Role::STORE_MANAGER));
        $this->assertTrue($this->session->mayRole(Role::AMBASSADOR));
        $this->assertFalse($this->session->mayRole(Role::ORGA));
        $this->assertFalse($this->session->mayRole(Role::SITE_ADMIN));

        $this->session->setTestUser(Role::STORE_MANAGER);
        $this->assertTrue($this->session->mayRole(Role::FOODSHARER));
        $this->assertTrue($this->session->mayRole(Role::FOODSAVER));
        $this->assertTrue($this->session->mayRole(Role::STORE_MANAGER));
        $this->assertFalse($this->session->mayRole(Role::AMBASSADOR));
        $this->assertFalse($this->session->mayRole(Role::ORGA));
        $this->assertFalse($this->session->mayRole(Role::SITE_ADMIN));

        $this->session->setTestUser(Role::FOODSAVER);
        $this->assertTrue($this->session->mayRole(Role::FOODSHARER));
        $this->assertTrue($this->session->mayRole(Role::FOODSAVER));
        $this->assertFalse($this->session->mayRole(Role::STORE_MANAGER));
        $this->assertFalse($this->session->mayRole(Role::AMBASSADOR));
        $this->assertFalse($this->session->mayRole(Role::ORGA));
        $this->assertFalse($this->session->mayRole(Role::SITE_ADMIN));

        $this->session->setTestUser(Role::FOODSHARER);
        $this->assertTrue($this->session->mayRole(Role::FOODSHARER));
        $this->assertFalse($this->session->mayRole(Role::FOODSAVER));
        $this->assertFalse($this->session->mayRole(Role::STORE_MANAGER));
        $this->assertFalse($this->session->mayRole(Role::AMBASSADOR));
        $this->assertFalse($this->session->mayRole(Role::ORGA));
        $this->assertFalse($this->session->mayRole(Role::SITE_ADMIN));
    }
}
