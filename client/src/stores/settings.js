export const SUB_PAGE = Object.freeze({
  GENERAL: 'general',
  NOTIFICATION: 'info',
  BUSINESS_CARD: 'bcard',
  CALENDAR: 'calendar',
  PASSPORT: 'passport',
  SLEEPING: 'sleeping',
  CHANGE_EMAIL: 'changeEmail',
  DELETE_ACCOUNT: 'deleteaccount',
  QUIZ: 'rise_role',
  HYGIENE: 'hygiene',
})
