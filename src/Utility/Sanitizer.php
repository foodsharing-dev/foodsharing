<?php

namespace Foodsharing\Utility;

use Html2Text\Html2Text;
use HTMLPurifier;
use HTMLPurifier_Config;
use Parsedown;

class Sanitizer
{
    private readonly Parsedown $parseDown;
    private readonly HTMLPurifier $htmlPurifier;

    public function __construct(Parsedown $parseDown, HTMLPurifier $HTMLPurifier)
    {
        $this->parseDown = $parseDown;
        $this->htmlPurifier = $HTMLPurifier;
    }

    public function plainToHtml(string $text): string
    {
        return nl2br(htmlspecialchars($text));
    }

    public function markdownToHtml(string $text): string
    {
        $html = $this->parseDown->text($text);

        return $this->htmlPurifier->purify($html);
    }

    public function purifyHtml(string $html, ?HTMLPurifier_Config $config = null): string
    {
        return $this->htmlPurifier->purify($html, $config);
    }

    public function htmlToPlain(string $html): string
    {
        $html = new Html2Text($html);

        return $html->getText();
    }

    public function tagSelectIds(array $v): array
    {
        $result = [];
        foreach ($v as $idKey => $value) {
            $result[] = explode('-', $idKey)[0];
        }

        return $result;
    }

    public function jsSafe(string $str, string $quote = "'"): string
    {
        return str_replace([$quote, "\n", "\r"], ['\\' . $quote . '', '\\n', ''], $str);
    }

    /* this method returns the string $str truncated to the first $length characters while keeping words intact.
       It adds ' ...' as an ellipsis. */
    public function tt(string $str, int $length = 160): string
    {
        if (mb_strlen($str) > $length) {
            $shortened = mb_substr($str, 0, $length - 4);
            $followingChar = mb_substr($str, $length - 4, 1);
            if (preg_match('/\s/', $followingChar) !== false) {
                /* following char is whitespace -> last word is complete but might have whitespace at the end */
                return preg_replace('/\s$/', '', $shortened) . ' ...';
            }

            /* word is broken: remove it */
            return preg_replace('/\s?\S*$/', '', $shortened) . ' ...';
        }

        return $str;
    }

    /**
     * Creates a configuration for the HTMLPurifier that uses a valid cache directory. Use this if you need a custom
     * configuration for the purifier, for example for including pictures.
     *
     * This is a workaround for a bug in the library which tries to write to the vendor directory. It can be removed
     * as soon as they fixed that. https://github.com/ezyang/htmlpurifier/issues/71
     */
    public static function getPurifierConfig(): HTMLPurifier_Config
    {
        $cacheDir = sys_get_temp_dir() . '/HTMLPurifier/DefinitionCache';
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir, 0770, true);
        }

        $config = HTMLPurifier_Config::createDefault();
        $config->set('Cache.SerializerPath', $cacheDir);

        return $config;
    }
}
