<?php

namespace Foodsharing\Modules\Core\DBConstants\Map;

class MapConstants
{
    final public const float CENTER_GERMANY_LAT = 50.89;
    final public const float CENTER_GERMANY_LON = 10.13;
    final public const int ZOOM_COUNTRY = 6;
    final public const int ZOOM_CITY = 13;
}
