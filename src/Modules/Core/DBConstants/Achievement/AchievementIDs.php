<?php

namespace Foodsharing\Modules\Core\DBConstants\Achievement;

class AchievementIDs
{
    final public const int KAM_CERTIFICATE = 1;
    final public const int HYGIENE_CERTIFICATE = 4;
}
