<?php

namespace Foodsharing\Modules\Core\DBConstants\Region;

/**
 * Status of forum threads. Column 'status' in 'fs_theme'.
 */
class ThreadStatus
{
    final public const int OPEN = 0;
    final public const int CLOSED = 1;
}
