<?php

// table StoreSettings

namespace Foodsharing\Modules\Core\DBConstants\Store;

class StoreSettings
{
    final public const int USE_PICKUP_RULE_YES = 1;
    final public const int USE_PICKUP_RULE_NO = 0;
    final public const int PRESS_YES = 1;
    final public const int PRESS_NO = 0;
}
