<?php

// table fs_theme_follower

namespace Foodsharing\Modules\Core\DBConstants\Info;

/**
 * Following status for forum threads.
 */
class FollowStatus
{
    final public const int DISABLED = 0;
    final public const int ENABLED = 1;
}
