<?php

// tables fs_fairteiler_follower, fs_theme_follower

namespace Foodsharing\Modules\Core\DBConstants\Info;

/**
 * Following type for food share points or threads.
 */
class InfoType
{
    final public const int NONE = 0;
    final public const int EMAIL = 1;
    final public const int BELL = 2;
}
