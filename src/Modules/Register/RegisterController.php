<?php

namespace Foodsharing\Modules\Register;

use Foodsharing\Lib\FoodsharingController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class RegisterController extends FoodsharingController
{
    public function __construct()
    {
        parent::__construct();
    }

    #[Route(path: '/register', name: 'register')]
    public function index(): Response
    {
        if ($this->session->mayRole()) {
            $this->flashMessageHelper->info($this->translator->trans('register.account-exists'));

            return $this->redirectToRoute('dashboard');
        } else {
            $this->pageHelper->addBread($this->translator->trans('register.title'));
            $this->pageHelper->addTitle($this->translator->trans('register.title'));

            $this->pageHelper->addContent($this->prepareVueComponent('register-form', 'RegisterForm'));
        }

        return $this->renderGlobal();
    }
}
