<?php

namespace Foodsharing\Modules\FoodSharePoint;

use Foodsharing\Lib\Session;
use Foodsharing\Modules\Core\DBConstants\Info\InfoType;
use Foodsharing\Modules\Core\DBConstants\Uploads\UploadUsage;
use Foodsharing\Modules\Uploads\UploadsGateway;
use Foodsharing\Modules\Uploads\UploadsTransactions;
use Foodsharing\Permissions\FoodSharePointPermissions;
use Foodsharing\RestApi\Models\FoodSharePoint\AddFoodSharePointResponse;
use Foodsharing\RestApi\Models\FoodSharePoint\FoodSharePointEditData;
use Foodsharing\RestApi\Models\FoodSharePoint\FoodSharePointForCreation;
use Foodsharing\RestApi\Models\Notifications\FoodSharePoint;
use Foodsharing\Utility\EmailHelper;
use Symfony\Contracts\Translation\TranslatorInterface;

class FoodSharePointTransactions
{
    public function __construct(
        private readonly FoodSharePointGateway $foodSharePointGateway,
        private readonly FoodSharePointPermissions $foodSharePointPermissions,
        private readonly UploadsGateway $uploadsGateway,
        private readonly UploadsTransactions $uploadsTransactions,
        private readonly EmailHelper $emailHelper,
        private readonly TranslatorInterface $translator,
        private readonly Session $session
    ) {
    }

    public function sendNewFoodSharePointMailNotifications(int $foodSharePointId): void
    {
        if ($foodSharePoint = $this->foodSharePointGateway->getFoodSharePoint($foodSharePointId)) {
            $post = $this->foodSharePointGateway->getLastFoodSharePointPost($foodSharePointId);
            if ($followers = $this->foodSharePointGateway->getEmailFollower($foodSharePointId)) {
                $body = nl2br((string)$post['body']);

                if (!empty($post['attach'])) {
                    $attach = json_decode((string)$post['attach'], true);
                    if (isset($attach['image']) && !empty($attach['image'])) {
                        foreach ($attach['image'] as $img) {
                            $body .= '
							<div>
								<img src="' . BASE_URL . '/images/wallpost/medium_' . $img['file'] . '" />
							</div>';
                        }
                    }
                }

                $followersWithoutPostAuthor = array_filter($followers, fn ($x) => $x['id'] !== $post['fs_id']);
                foreach ($followersWithoutPostAuthor as $f) {
                    $this->emailHelper->tplMail('foodSharePoint/new_message', $f['email'], [
                        'link' => BASE_URL . '/fairteiler/' . (int)$foodSharePointId,
                        'name' => $f['name'],
                        'anrede' => $this->translator->trans('salutation.' . $f['geschlecht']),
                        'fairteiler' => $foodSharePoint['name'],
                        'post' => $body
                    ]);
                }
            }
        }
    }

    /**
     * Updates the user's notification settings for a list of food share points individually.
     *
     * @param FoodSharePoint[] $foodSharePoints
     */
    public function updateFoodSharePointNotifications(int $userId, array $foodSharePoints): void
    {
        foreach ($foodSharePoints as $foodSharePoint) {
            $foodSharePointIdsToUnfollow = [];

            if ($foodSharePoint->infotype == InfoType::NONE) {
                $foodSharePointIdsToUnfollow[] = $foodSharePoint->id;
            }
            $this->foodSharePointGateway->updateInfoType($userId, $foodSharePoint->id, $foodSharePoint->infotype);
        }

        if (!empty($foodSharePointIdsToUnfollow)) {
            $this->foodSharePointGateway->unfollowFoodSharePoints($userId, $foodSharePointIdsToUnfollow);
        }
    }

    /**
     * Adds a new food share point. If the user is not allowed to add a food share point to that region, it will be
     * suggested and needs to be approved by someone responsible.
     *
     * @param FoodSharePointForCreation $data initial data for the FSP
     * @return AddFoodSharePointResponse information about the created food share point
     */
    public function addFoodSharePoint(FoodSharePointForCreation $data): AddFoodSharePointResponse
    {
        $isProposal = !$this->foodSharePointPermissions->mayAdd($data->regionId);
        $id = $this->foodSharePointGateway->addFoodSharePoint($this->session->id(), $data, $isProposal);

        // If a picture was uploaded for this food share point, its usage type needs to be set
        if (!empty($data->picture)) {
            $uuid = substr($data->picture, 13);
            $this->uploadsGateway->setUsage([$uuid], UploadUsage::FOOD_SHARE_POINT_TITLE, $id);
        }

        return new AddFoodSharePointResponse($id, !$isProposal);
    }

    public function editFoodSharePoint(int $foodSharePointId, array $currentData, FoodSharePointEditData $newData): void
    {
        $this->foodSharePointGateway->updateFoodSharePoint($foodSharePointId, $newData);

        /* If the picture of this food share point was changed, the usage type of the new one (if any) needs to be set
         and the old picture needs to be deleted. */
        $newPicture = $newData->picture ?? '';
        if ($newPicture !== $currentData['picture']) {
            if (!empty($currentData['picture'])) {
                $oldUUID = substr((string)$currentData['picture'], 13);
                $this->uploadsTransactions->deleteUploadedFile($oldUUID);
            }

            if (!empty($newPicture)) {
                $uuid = substr($newPicture, 13);
                $this->uploadsGateway->setUsage([$uuid], UploadUsage::FOOD_SHARE_POINT_TITLE, $foodSharePointId);
            }
        }

        $this->foodSharePointGateway->updateFSPManagers($currentData['id'], $newData->managerIds);
    }
}
