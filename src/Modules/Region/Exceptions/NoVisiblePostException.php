<?php

namespace Foodsharing\Modules\Region\Exceptions;

class NoVisiblePostException extends \Exception
{
}
