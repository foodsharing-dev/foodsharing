<?php

namespace Foodsharing\Modules\Store;

class TeamStatus
{
    final public const int NoMember = 0;
    final public const int Applied = 1;
    final public const int WaitingList = 2;
    final public const int Member = 3;
    final public const int Coordinator = 4;
}
