<?php

namespace Foodsharing\RestApi\Models\FoodSharePoint;

class FoodSharePointPermission
{
    public bool $isFollower;
    public bool $mayEdit;
    public bool $mayDelete;
}
