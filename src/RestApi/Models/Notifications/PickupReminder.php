<?php

namespace Foodsharing\RestApi\Models\Notifications;

class PickupReminder
{
    /**
     * Enable or disable reminder mails.
     */
    public bool $sendMail;
}
