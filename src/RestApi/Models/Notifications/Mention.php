<?php

namespace Foodsharing\RestApi\Models\Notifications;

class Mention
{
    /**
     * Enable or disable mentioning Notifications.
     */
    public bool $mention;
}
