---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Foodsharing Devdocs"
  text: "Schreibe Code, Rette Lebensmittel"
  tagline: "Lebensmittelrettung mit jeder Zeile Code"
  actions:
    - theme: brand
      text: Erste Schritte
      link: /de/getting-started
    - theme: alt
      text: Einführung
      link: /de/intro
  image:
    light: /img/FS_Gabel_gb.svg
    dark: /img/FS_Gabel_gw.svg
    alt: Foodsharing Logo

features:
  - title: Vue Frontend
    icon: 
      src: /img/icons/vue.svg
    details: Wir verwenden Vue.js für seine reaktive und komponentenbasierte Architektur, die hoch interaktive und wartbare Benutzeroberflächen ermöglicht.
  - title: Symfony Backend
    icon: 
      src: /img/icons/symfony.svg
    details: PHP mit Symfony wurde aufgrund seines leistungsstarken Frameworks gewählt, das komplexe Aufgaben vereinfacht, bewährte Praktiken fördert und eine schnelle Entwicklung mit wiederverwendbaren Komponenten sicherstellt.
  - title: Bootstrap
    icon: 
      src: /img/icons/bootstrap.svg
    details: Bootstrap wird aufgrund seines responsiven Design-Frameworks verwendet, das eine konsistente und mobilfreundliche Benutzererfahrung auf allen Geräten bietet.
---

