---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Foodsharing Devdocs"
  text: "Code. Save Food."
  tagline: "Empowering Foodsharing with Every Line"
  actions:
    - theme: brand
      text: Getting Started
      link: /getting-started
    - theme: alt
      text: Introduction
      link: /intro
  image:
    light: /img/FS_Gabel_gb.svg
    dark: /img/FS_Gabel_gw.svg
    alt: Foodsharing logo

features:
  - title: Vue Frontend
    icon: 
      src:  /img/icons/vue.svg
    details: We use Vue.js for its reactive and component-based architecture, allowing for highly interactive and maintainable user interfaces.
  - title: Symfony Backend
    icon: 
      src: /img/icons/symfony.svg
    details: PHP with Symfony is chosen for its powerful framework that simplifies complex tasks, promotes best practices, and ensures fast development with reusable components.
  - title: Bootstrap
    icon: 
      src: /img/icons/bootstrap.svg
    details:  Bootstrap is utilized for its responsive design framework, providing a consistent and mobile-friendly user experience across all devices.
---
