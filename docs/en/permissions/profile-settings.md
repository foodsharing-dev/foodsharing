# profile settings permissions

| Permission                                                                                        | Self (FOODSHARER) | Self (FOODSAVER) | Ambassador (for user's region) | Orga |
|---------------------------------------------------------------------------------------------------|-------------------|------------------|--------------------------------|------|
| Can Change Name                                                                                   | Yes               | No               | Yes                            | Yes  |
| Can Change Home Region                                                                            | No                | No               | Yes                            | Yes  |
| Can Change Role                                                                                   | No                | No               | No                             | Yes  |
| Can Change gender, birthdate, phone numbers, address and geo location and noAutoDelete Flag       | Yes               | Yes              | Yes                            | Yes  |
| Can Change aboutMeIntern                                                                          | Yes               | Yes              | No                             | Yes  |
| Can Change position and aboutMePublic on global team page if the user is in the global team group | Yes               | Yes              | No                             | Yes  |
